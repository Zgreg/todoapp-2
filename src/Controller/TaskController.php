<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TaskController extends AbstractController
{
    /**
     * @var TaskRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * Constructor of TaskController for dependency injection.
     *
     * @param TaskRepository $repository
     * @param EntityManagerInterface $manager
     */
    public function __construct(TaskRepository $repository, EntityManagerInterface $manager) {
        $this->repository = $repository;
        $this->manager = $manager;
    }


    /**
     * Display the task list.
     * 
     * @Route("/tasks/listing", name="tasks_listing")
     * 
     * @return Response
     */
    public function taskListing(): Response
    {

        // On va chercher par doctrine le repository de nos Task
        $repository = $this->getDoctrine()->getRepository(Task::class);
        
        // On récupère toutes les données Task
        $tasks = $repository->findAll();

        // dd($tasks);

        return $this->render('task/index.html.twig', [
            'tasks' => $tasks
        ]);
    }

    // /**
    //  * Create a new task.
    //  * 
    //  * @Route("/tasks/create", name="tasks_create")
    //  * 
    //  * @param Request $request
    //  * @return void
    //  */
    // public function createTask(Request $request) 
    // {
    //     $task = new Task();

    //     $task->setCreatedAt(new \DateTime());

    //     $form = $this->createForm(TaskType::class, $task, []);

    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() and $form->isValid()) 
    //     {
    //         $manager = $this->getDoctrine()->getManager();

    //         $task->setname($form['name']->getData())
    //             ->setDescription($form['description']->getData())
    //             ->setDueAt($form['dueAt']->getData())
    //             ->setTag($form['tag']->getData());

    //         $manager->persist($task);
    //         $manager->flush();

    //         return $this->redirectToRoute('tasks_listing');
    //     }

    //     return $this->render('task/create.html.twig', [
    //         'form' => $form->createView(),
    //     ]);
    // }

    // /**
    //  * Update a task.
    //  * 
    //  * @Route("/tasks/update/{id}", name="tasks_update", requirements={"id"="\d+"})
    //  * 
    //  * @param Int $id
    //  * @param Request $request
    //  * @return void
    //  */
    // public function updateTask($id, Request $request)
    // {
    //     $task = $this->getDoctrine()->getRepository(Task::class)->findOneBy(['id' => $id]);

    //     $form = $this->createForm(TaskType::class, $task, array());
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() and $form->isValid())
    //     {
    //         $manager = $this->getDoctrine()->getManager();

    //         $task->setname($form['name']->getData())
    //             ->setDescription($form['description']->getData())
    //             ->setDueAt($form['dueAt']->getData())
    //             ->setTag($form['tag']->getData());

    //         $manager->persist($task);
    //         $manager->flush();

    //         return $this->redirectToRoute('tasks_listing');
    //     }

    //     return $this->render('task/create.html.twig', [
    //         'task' => $task,
    //         'form' => $form->createView(),
    //     ]);
    // }

    /**
     * Create and update task
     *
     * @Route("/tasks/create", name="tasks_create")
     * @Route("/tasks/update/{id}", name="tasks_update", requirements={"id"="\d+"})
     * 
     * @param Task $task
     * @param Request $request
     * @return Response
     */
    public function task(Task $task = null, Request $request) : Response 
    {
        if (!$task) {
            $task = new Task();
            $flag = false;
        } else {
            $flag = true;
        }
        $form = $this->createForm(TaskType::class, $task, array());
        if (!$flag) {
            $task->setCreatedAt(new \DateTime());
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isvalid()) {
            $task->setname($form['name']->getData())
                ->setDescription($form['description']->getData())
                ->setDueAt($form['dueAt']->getData())
                ->setTag($form['tag']->getData());

            $this->manager->persist($task);
            $this->manager->flush();

            return $this->redirectToRoute('tasks_listing');
        }

        return $this->render('task/create.html.twig', array(
            'task' => $task, 
            'form' => $form->createView()
        ));
    }


    /**
     * Delete Task.
     * 
     * @Route("/tasks/delete/{id}", name="tasks_delete", requirements={"id"="\d+"})
     * 
     * @param Int $id
     * @return Response
     */
    public function deleteTask($id) 
    {
        $manager = $this->getDoctrine()->getManager();
        $task = $this->getDoctrine()->getRepository(Task::class)->findOneBy(['id' => $id]);

        $manager->remove($task);
        $manager->flush();

        return $this->redirectToRoute('tasks_listing');
    }

}
