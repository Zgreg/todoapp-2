<?php
namespace App\DataFixtures;
use Faker\Factory;
use App\Entity\Tag;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // Création d'un nouvel objet Faker
        $faker = Factory::create('fr_FR');
        // création de nos 5 catégories
        for ($c = 0; $c < 5; $c++) {
            // création d'un nouvel objet Tag
            $tag = new Tag;
            // On ajoute un nom à notre catégorie
            $tag->setName($faker->colorName());
            // On fait persister les données
            $manager->persist($tag);
        }
        // On push les catégories en BDD
        $manager->flush();

        // récupération des catégories créées
        $allTags = $manager->getRepository(Tag::class)->findAll();

        // Création entre 15 et 30 tâches aléatoirement
        for ($t = 0; $t < mt_rand(15, 30); $t++) {
            // Création d'un nouvel objet Task
            $task = new Task;
            // On nourrit l'objet Task
            $task->setName($faker->sentence(6))
                ->setDescription($faker->paragraph(3))
                ->setCreatedAt(new \Datetime())
                ->setDueAt($faker->dateTimeBetween('now', '6 months'))
                ->setTag($faker->randomElement($allTags));
            // On fait persister les données
            $manager->persist($task);
        }
        // On push les tâches en BDD
        $manager->flush();

        // Création d'un nouvel utilisateur
        for ($u = 0; $u < 5; $u++) {
            $user = new User;

            //Hashage password avec parametre de sécurité de $user
            $hash = $this->encoder->encodePassword($user, "password");

            $user->setEmail($faker->safeEmail())
                ->setPassword($hash);

            // si c'est le premier utilisateur, on lui donne le rôle admin
            if ($u === 0) {
                $user->setRoles(['ROLE_ADMIN']);
            }

            // On fait persister les données
            $manager->persist($user);
        }
        // On push les users en BDD
        $manager->flush();
    }
}